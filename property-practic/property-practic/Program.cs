﻿using System;

namespace property_practic
{

    class Program
    {
        static void Main(string[] args)
        {
            var Masha = new Student();
            //set
            Masha.Name = "Маша";
            //get
            string zxc = Masha.Name;

        }
    }

    class Student
    {
        private string name;

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

    }

    class Dota
    {
        private int age;
        public int Age
        {
            set
            {
                if (value < 18)

                {
                    Console.WriteLine("Ты школяр!");
                }
                else
                {
                    age = value;
                }
            }
            get
            {
                return age;
            }
        }
    }
    class Person
    {
        private string name;
        // свойство только для чтения
        public string Name
        {
            get
            {
                return name;
            }
        }

        private int age;
        // свойство только для записи
        public int Age
        {
            set
            {
                age = value;
            }
        }
    }

    /// <summary>
    ///Сокращенные свойства
    /// </summary>
    class Test0Aut
    {
        public string Name { get; set; }
        public int Age { get; set; }

        public Test0Aut(string name, int age)
        {
            Name = name;
            Age = age;
        }
    }

    //Значение по умолчанию
    class Vogue
    {
        public string Name { get; set; } = "Tom";
        public int Age { get; set; } = 23;
    }


    class ShortWrite
    {
        private string name;

        // эквивалентно public string Name { get { return name; } }
        public string Name => name;
    }
}
